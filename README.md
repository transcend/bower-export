# Export Module
The "Export" module provides a set of functionality to export data to different formats. Currently the only export type
in this module is CSV - however additional types will be added in the future.

## Installation
The easiest way to use this, and other Transcend libraries, is to use [Bower.js](http://bower.io/). Run the following
commands to install this package:

```
bower install https://bitbucket.org/transcend/bower-export.git
```

Other options:

* Download the code at [http://code.tsstools.com/bower-export/get/master.zip](http://code.tsstools.com/bower-export/get/master.zip)
* View the repository at [http://code.tsstools.com/bower-export](http://code.tsstools.com/bower-export)

When you have pulled down the code and included the files (and dependency files), simply add the module as a
dependency to your application:

```
angular.module('myModule', ['transcend.export']);
```

## To Do
- Implement additional export types (other than CSV).