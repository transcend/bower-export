/**
 * @ngdoc overview
 * @name transcend.export
 * @description
 # Export Module
 The "Export" module provides a set of functionality to export data to different formats. Currently the only export type
 in this module is CSV - however additional types will be added in the future.

 ## Installation
 The easiest way to use this, and other Transcend libraries, is to use [Bower.js](http://bower.io/). Run the following
 commands to install this package:

 ```
 bower install https://bitbucket.org/transcend/bower-export.git
 ```

 Other options:

 * Download the code at [http://code.tsstools.com/bower-export/get/master.zip](http://code.tsstools.com/bower-export/get/master.zip)
 * View the repository at [http://code.tsstools.com/bower-export](http://code.tsstools.com/bower-export)

 When you have pulled down the code and included the files (and dependency files), simply add the module as a
 dependency to your application:

 ```
 angular.module('myModule', ['transcend.export']);
 ```

 ## To Do
 - Implement additional export types (other than CSV).
 */
/**
 * @ngdoc object
 * @name transcend.export.exportConfig
 *
 * @description
 * Provides a single configuration object that can be used to change the behaviour, options, urls, etc for any
 * components in the {@link transcend.export Export Module}. The "exportConfig" object is a
 * {@link http://docs.angularjs.org/api/auto/object/$provide#value module value} and can therefore be
 * overridden/configured by:
 *
 <pre>
 // Override the value of the 'exportConfig' object.
 angular.module('myApp').value('exportConfig', {
     resource: {
       url: 'http://localhost/MyWebApiApp'
     }
   });
 </pre>
 **/
/**
 * @ngdoc object
 * @name transcend.export.exportConfig#resource
 * @propertyOf transcend.export.exportConfig
 *
 * @description
 * Configuration object to configure common resource properties, such as the server URL and default parameters.
 * All resources in the "{@link transcend.export Export Module}" extend the resource config object first, and
 * then extend the resource's specific configuration. So you can set the default backend server URL for all
 * resources, by using this configuration, and define any other configurations that may vary as well.
 *
 * ## Configuration Properties
 *   * __url__ - a string that that represents the base url to the backend server.
 *   * __params__ - default URL parameters that will be passed to each service call.
 *
 <pre>
 angular.module('myApp').value('exportConfig', {
       // Any resource's URL will be set
       // by the resource url configuration.
       resource: {
         url: 'http://localhost/MyWebApiApplication'
       },
       // The "$download" resource's URL will initially be set by
       // the resource url configuration, but
       // will be overridden by the "download" configuration.
       // So ultimately the "$download" resource URL will
       // be 'http://otherhost/MyOtherApplication'.
       download: {
         url: 'http://otherhost/MyOtherApplication'
       }
     });
 </pre>
 */
/**
 * @ngdoc service
 * @name transcend.export.$download
 *
 * @description
 * The '$download' service provides the ability to download content. It is specifically useful to initiate a download
 * of a file across all major browsers. The problem is that you can not initiate a download of a file directly from
 * client side code in all browsers (IE in particular). This service will upload the content by submitting it as a form,
 * behind the scenes, and initiating a download on the response.
 *
 * @requires $document
 * @requires $log
 * @requires transcend.core.$string
 <pre>
 // Auto-detect format from extension.
 $download({}, 'my-file.json');       // -> downloads: 'my-file.json'
 $download('foo,bar', 'my-file.csv'); // -> downloads: 'my-file.csv'
 $download('foo', 'my-file.txt');     // -> downloads: 'my-file.txt'

 // Specifically specify a format/extension type.
 $download.json({}, 'my-file');       // -> downloads: 'my-file.json'
 $download.csv('foo,bar', 'my-file'); // -> downloads: 'my-file.csv'
 $download.txt('foo', 'my-file');     // -> downloads: 'my-file.txt'
 </pre>
 *
 * @param {*} data The data/content to download.
 * @param {String=} file The name of the file to download. This can include an extension, but doesn't have to.
 * @param {String=} type The type of download (csv, json, txt, etc).
 * @param {String=} mediaType The media-type that wil be used in the response header (text/csv, json/application, etc).
 * This is optional and if left blank will be populated based on the passed in 'type' or 'extension'.
 * @param {String=} disposition The disposition text to send back in the header.
 * @param {String=} method The type of method the form should submit. By default it is a 'POST'.
 **/
/**
       * @ngdoc object
       * @name transcend.export.exportConfig#download
       * @propertyOf transcend.export.exportConfig
       *
       * @description
       * Configuration object for the {@link transcend.export:$download $download service}. This object provides
       * the capability to override the {@link transcend.export:$download $download service's} default
       * configuration.
       *
       * ## Configuration Properties
       *   * __url__ - a string that that represents the base url to the backend server.
       *
       <pre>
       angular.module('myApp').value('exportConfig', {
        download: {
          url: 'http://localhost/download'
        }
       });
       </pre>
       */
/**
       *
       * @ngdoc method
       * @name transcend.export.extension
       * @propertyOf transcend.esri.$arcgis
       *
       * @description
       * Extracts an extension from a file name.
       *
       * @param {String} fileName The file name.
       * @param {String=} defaultExtension A default extension to return if an extension is not found.
       * @returns {String} The extension.
       */
/**
       *
       * @ngdoc method
       * @name transcend.export.fileName
       * @propertyOf transcend.export.$download
       *
       * @description
       * Gets a file name based on a given file name with or without an extension. This will essentially extract the
       * extension and use it if it finds one, otherwise add a default extension to it.
       *
       * @param {String} fileName The file name.
       * @param {String=} defaultExtension A default extension to add to the file name if there is not already an
       * extension found in the file name.
       * @returns {String} The file name.
       */
/**
       * The actual download functionality all lives in this method. All the other methods
       * are meant to transform the input into the correct parameters for this method to use.
       *
       * @param {*} data The data/content to download.
       * @param {String=} file The name of the file to download. This can include an extension, but doesn't have to.
       * @param {String=} type The type of download (csv, json, txt, etc).
       * @param {String=} mediaType The media-type that wil be used in the response header (text/csv, json/application, etc).
       * This is optional and if left blank will be populated based on the passed in 'type' or 'extension'.
       * @param {String=} disposition The disposition text to send back in the header.
       * @param {String=} method The type of method the form should submit. By default it is a 'POST'.
       */
/**
     * @ngdoc method
     * @name transcend.export.$download#json
     * @methodOf transcend.export.$download
     *
     * @description
     * Downloads content specifically as 'JSON' media type. If an object is passed in it will be serialized to JSON.
     *
     * @param {*} data The data/content to download.
     * @param {String=} fileOrExt The name of the file to download. This can include an extension, but doesn't have to.
     *
     * @example
     <pre>
     $download.json({}, 'my-file');
     $download.json('{}', 'my-file')
     </pre>
     */
/**
     * @ngdoc method
     * @name transcend.export.$download#csv
     * @methodOf transcend.export.$download
     *
     * @description
     * Downloads content specifically as 'csv' media type. If an object is passed in it will be serialized to csv.
     *
     * @param {*} data The data/content to download.
     * @param {String=} fileOrExt The name of the file to download. This can include an extension, but doesn't have to.
     *
     * @example
     <pre>
     $download.csv({}, 'my-file');
     $download.csv('foo,bar', 'my-file')
     </pre>
     */
/**
     * @ngdoc method
     * @name transcend.export.$download#txt
     * @methodOf transcend.export.$download
     *
     * @description
     * Downloads content specifically as 'text' media type. If an object is passed in it will use the string
     * representation of the object (toString method).
     *
     * @param {*} data The data/content to download.
     * @param {String=} fileOrExt The name of the file to download. This can include an extension, but doesn't have to.
     *
     * @example
     <pre>
     $download.txt({}, 'my-file');
     $download.txt('foo', 'my-file')
     </pre>
     */
/**
 * @ngdoc service
 * @name transcend.export.$csv
 *
 * @description
 * Provides a set of utility functions related to csv conversion. These methods are 'static' functions found
 * on the factory object.
 *
 <h3>Calling Static CSV Conversion Utility Functions</h3>
 <pre>
 $csv.someMethod(args);
 </pre>
 */
/**
     * @ngdoc method
     * @name transcend.export.$csv.stringify
     * @methodOf transcend.export.$csv
     *
     * @description
     * Returns a string representation of a value passed into the function.  This method does not operate on
     * complex values, such as arrays.
     *
     * @param {String} value The value to convert to a string representation.  The value can be of type boolean,
     *   string, or number.
     * @returns {String} Returns the resulting string value
     * @example
     <pre>
     // Convert values to a string
     expect($csv.stringify('def')).toEqual('def');
     expect($csv.stringify2(true)).toEqual('TRUE');
     expect($csv.stringify2(23)).toEqual('23');
     </pre>
     */
/**
     * @ngdoc method
     * @name transcend.export.$csv.swapLastCommaForNewLine
     * @methodOf transcend.export.$csv
     *
     * @description
     * If the value passed in is a string that ends with a comma, the last character of the string is replaced
     * with a newline (\n) character and the resulting string is returned.  Otherwise, the original value is
     * returned.
     *
     * @param {String} str The value to be modified.
     * @returns {String} Returns the modified string if it meets the conditions; otherwise returns the original
     *   value
     * @example
     <pre>
     // Replace the ending comma in a string with a newline
     expect($csv.swapLastCommaForNewline('def,')).toEqual('def\n');
     expect($csv.swapLastCommaForNewline(true)).toEqual(true);
     expect($csv.swapLastCommaForNewline(124)).toEqual(124);
     </pre>
     */
/**
     * @ngdoc method
     * @name transcend.export.$csv.buildCSVString
     * @methodOf transcend.export.$csv
     *
     * @description
     * Return a list of objects(key-value pairs) formatted as a csv string.  If the value passed in is not a list
     * of key-value pairs, a null string ('') is returned.
     *
     * @param {String} data The list of key-value pairs to be formatted
     * @returns {String} Returns the list of key-value pairs in csv string format; if not a list of key-value pairs,
     *   returns a null string
     * @example
     <pre>
     // Create a csv string out of a list of key-value pairs
     expect($csv.buildCSVString(data)).toEqual('"a","b","c","d"\n"1","2","3",""\n"2","3","4","TRUE"\n"3","4","5","FALSE"');
     expect($csv.buildCSVString(data[0])).toEqual('');
     </pre>
     */
